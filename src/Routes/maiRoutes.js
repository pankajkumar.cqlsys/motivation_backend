const express =require('express')
const router=express.Router()
const multer = require('multer')
const stroageData=require('../utils/fileUpload')
const {create_mood,get_moodData, deleteMood, get_data_with_id, mood_update, put_upadte, mood_edit}=require('../controller/moodController')
const { createUser, LoginUser } = require('../controller/userController')
const { authenticateToken } = require('../controller/authenticate')
router.get('/',(req,res)=>{
    res.send("helo")
})

const upload=multer({storage:stroageData.fileUpload("mood")})
router.post('/create_mood',upload.single('image'),create_mood)
router.post('/create_user',createUser)
router.post('/login', LoginUser)

// put
const uploadUpdate=multer({storage:stroageData.fileUpload("mood")})
router.put('/mood_update/:id',uploadUpdate.single('image'),mood_update)
router.put('/mood_edit/:id',mood_edit)


// get api
router.get('/get_mood_data',authenticateToken,get_moodData)
router.get('/getuniquedata/:id',get_data_with_id)

// delete api
router.delete('/delete_mood/:id', deleteMood)

module.exports=router