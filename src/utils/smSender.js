const axios = require('axios');
 async function sendSMS(to) {
    const exotelSid = 'cqlsys1';
    const exotelToken = '2bae334620f24519882a2878b752d8aa2978d332f5013950';
    const exophoneNumber = '08115809072';
    const message = 'Hello from Exotel! This is your verification code: 1234'
    const baseUrl = `https://api.exotel.com/v1/Accounts/${exotelSid}/Sms/send.json`;

    try {
        const response = await axios.post(baseUrl, {
            From: exophoneNumber,
            To: "8874705546",
            Body:"helloo"
        }, {
            auth: {
                username: exotelSid,
                password: exotelToken
            }
        });

        console.log('SMS sent successfully:', response);
        return response.data;
    } catch (error) {
        console.error('Error sending SMS:', error.response.data);
        throw error.response.data;
    }
}

module.exports = sendSMS;
