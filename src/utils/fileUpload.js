// const multer = require("multer");
// exports.fileUpload = (pathname) => {
//   console.log(pathname,"p")
//   const storage = multer.diskStorage({
//     destination: `/src/uploads/${pathname}`,
//       filename: (req, file, callback) => {
     
//         callback(null, file.originalname);
//       }
    
//   });
//   return storage
// };
const multer = require("multer");
const fs = require("fs");

exports.fileUpload = (pathname) => {
  const storage = multer.diskStorage({
    destination: function (req, file, callback) {
      // Check if the directory exists or create it
      const dir = `./src/uploads/${pathname}`;
      try {
        fs.mkdirSync(dir, { recursive: true });
        callback(null, dir);
      } catch (err) {
        console.error("Error creating directory:", err);
        callback(err, null);
      }
    },
    filename: function (req, file, callback) {
      callback(null,file.fieldname+'_'+file.originalname);
    }
  });
  return storage;
};

