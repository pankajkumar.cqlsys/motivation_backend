const express = require('express');
const app = express();
const Router = require('./Routes/maiRoutes');
const cors = require("cors");
require('dotenv').config();
require('./Database/db').apply();
const path = require('path');
const port = process.env.SERVER_PORT || 5000;

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.use(cors());
app.use(express.json());

// Serve static images
app.use('/img', express.static(path.join(__dirname, './uploads/mood')))

// Define API routes
app.use('/api', Router);

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
