
const { verifyRefreshToken } = require("../utils/verifyToken");

exports.authenticateToken = (req, res, next) => {
    try {
        const authHeader = req.headers["authorization"];
        if (authHeader) {
            const bearer = authHeader.split(" ");
            const bearerToken = bearer[1];
            const result = verifyRefreshToken(bearerToken);
           
            if (result.success === false) {
                if (result.error === "jwt expired") {
                    return res.status(401).json({ success: false, msg: "Token expired" ,result});
                } else {
                    return res.status(403).json(result);
                }
            } else {
                next();
            }
        } else {
            return res.status(403).json({ success: false,msg:"token required" });
        }
    } catch (error) {
        return res.status(500).json({ success: false, msg: error.message });
    }
};
