const moodModel = require("../model/moodModel");
const userModel = require("../model/userModel");

exports.create_mood = async (req, res) => {
  try {
    const { Title, createdAt } = req.body;
    const createmood = await moodModel.create({
      Title,
      createdAt,
      image:req.file.filename
    });
    if (createmood) {
      return res.status(200).json({ status: true, createmood });
    }
  } catch (error) {
    return res.status(500).json({ status: false, msg: error.message });
  }
};


// get api 

exports.get_moodData=async(req,res)=>{

try {
    const get_data=await moodModel.find()
    if(get_data){
      return res.status(200).json({ status: true, get_data });
    }else{
      return res.status(404).json({ status: false, msg:"data not found" });
    }
} catch (error) {
    return res.status(500).json({ status: false, msg: error.message });
}
}

exports.deleteMood=async(req,res)=>{
 const {id}=req.params
  try {

    const del_data=await moodModel.findByIdAndDelete(id)
    if(del_data){
      return res.status(200).json({ status: true, del_data ,msg:"deleted data"});
    }else{
      return res.status(404).json({ status: false, msg:"data not found" });
    }
} catch (error) {
    return res.status(500).json({ status: false, msg: error.message });
}
}


exports.get_data_with_id=async(req,res)=>{
  const {id}=req.params

   try {
 
     const get_with_id=await moodModel.findById(id)
     if(get_with_id){
       return res.status(200).json({ status: true, get_with_id ,msg:"deleted data"});
     }else{
       return res.status(404).json({ status: false, msg:"data not found" });
     }
 } catch (error) {
     return res.status(500).json({ status: false, msg: error.message });
 }
 }

 
//  exports.mood_update = async(req, res) => {
//   const {id}=req.params
//   console.log(id)

//   await userModel.findByIdAndUpdate(
//     id,
//     {
//       $set: {
//         Title: req.body.Title,
//         image: req.file.filename
//       },
//     },
//     { new: true }
//   )
//   .then(updatedData => {
//     if (!updatedData) {
//       return res.status(404).json({ status: false, msg: "Data not found" });
//     }
//     res.json(updatedData);
//   })
//   .catch(error => {
//     res.status(500).json({ status: false, msg: error.message });
//   });
// };



 
exports.mood_edit=async(req,res)=>{
  const {id}=req.params
   try {
 
     const get_with_id=await moodModel.findByIdAndUpdate(id)
     if(get_with_id){
       return res.status(200).json({ status: true, get_with_id ,msg:"mood_edit data"});
     }else{
       return res.status(404).json({ status: false, msg:"data not found" });
     }
 } catch (error) {
     return res.status(500).json({ status: false, msg: error.message });
 }
 }

 exports.mood_update=async(req,res)=>{
  const {id}=req.params
  const {Title}=req.body

   try {
 
     const get_with_id=await moodModel.findByIdAndUpdate(id,{Title,image:req.file.filename})
     if(get_with_id){
       return res.status(200).json({ status: true, get_with_id ,msg:"deleted data"});
     }else{
       return res.status(404).json({ status: false, msg:"data not found" });
     }
 } catch (error) {
     return res.status(500).json({ status: false, msg: error.message });
 }
 }

