const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MoodSchems = new Schema({
  Title: { type: String, require: true },
  image: { type: String },
  createdAt: { type: String },
});
module.exports = new mongoose.model("mood", MoodSchems);
